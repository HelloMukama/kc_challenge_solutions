<?php
// CHALLENGE QUESTION 3,B
// Author, NTARYEBWAMUKAMA JEDIDIAH

function read_from_and_remove_duplicates($file_name) {
    // Read the file contents
    $text = file_get_contents($file_name);

    $punctuation_regex = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
    $text_chars = str_split($text);

    // Filter the array to keep only punctuation marks
    $punctuation_marks = array_filter($text_chars, function ($char) use ($punctuation_regex) {
        return strpos($punctuation_regex, $char) !== false;
    });

    // return array_values($punctuation_marks);  // maintain fixed indices
    return array_values($punctuation_marks);  // re-index array contents- new indices
}

$file_name = 'test-file.txt';
$punctuation_marks = read_from_and_remove_duplicates($file_name);

try {
    if (file_exists($file_name)) {  // Check if the file exists before processing
        if (is_readable($file_name)) {  // Check if the file is readable before processing
            $punctuation_marks = read_from_and_remove_duplicates($file_name);
            echo "\nPrinting Results of Non Duplicated contexts of the text file\n";
            print_r($punctuation_marks);
        } else {
            throw new Exception("The file '$file_name' is not readable.");
        }
    }else {
        throw new Exception("The file '$file_name' does not exist.");
    }
} catch (Exception $e) {
    echo "Error: " . $e->getMessage() . "\n";
}

?>
