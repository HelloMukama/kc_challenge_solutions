<?php
// CHALLENGE QUESTION 3,A
// Author, NTARYEBWAMUKAMA JEDIDIAH

function read_from_and_remove_duplicates($filename) {
    // Read the content of the file
    $content = file_get_contents($filename);

    // Split the content into an array of items
    $items = explode(" ", $content);

    // Scan for non-graphic & printable content- 'ctype_graph', remove duplicate items & non-characters, preserve original order
    // $non_dup_items = array_unique(array_filter($items, 'ctype_graph'));  // maintain fixed indices
    $non_dup_items = array_values(array_unique(array_filter($items, 'ctype_graph')));  // re-index array contents- new indices
    return $non_dup_items;
}

$file_name = 'test-file.txt';

try {
    if (file_exists($file_name)) {  // Check if the file exists before processing
        if (is_readable($file_name)) {  // Check if the file is readable before processing
            $result = read_from_and_remove_duplicates($file_name);
            echo "\nPrinting Results of Non Duplicated contexts of the text file\n";
            print_r($result);
        } else {
            throw new Exception("The file '$file_name' is not readable.");
        }
    }else {
        throw new Exception("The file '$file_name' does not exist.");
    }
} catch (Exception $e) {
    echo "Error: " . $e->getMessage() . "\n";
}

?>
