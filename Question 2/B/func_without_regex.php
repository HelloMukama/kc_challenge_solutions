<?php
// CHALLENGE QUESTION 2,B
// Author, NTARYEBWAMUKAMA JEDIDIAH

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Returning Email without Regex
function extract_email_address_without_regex($paragraph) {
    $words = explode(" ", $paragraph);
    foreach ($words as $word) {
        // Check if the word contains '@' and '.'
        if (strpos($word, "@") !== false && strpos($word, ".") !== false) {
            // Check if there is at least one character before and after '@'
            $at_position = strpos($word, "@");
            if ($at_position > 0 && $at_position < strlen($word) - 1) {
                return $word;
            }
        }
    }
    echo "\nEmail Address not found.\n";
    return null;
}

// Returning Phone Number without Regex
function extract_phone_number_without_regex($paragraph) {
    $digits = '';
    foreach (str_split($paragraph) as $char) {  // str_split()- convert $paragraph into an array of characters
        if (is_numeric($char)) {
            $digits .= $char;
        } elseif ($digits !== '') {
            $digits = ''; // Reset if non-digit is encountered after digits
        }
        if (strlen($digits) === 12) {
            return $digits;
        }
    }
    echo "\nPhone Number blank. Not Found\n";
    return null;
}

// Returning Url without Regex
function extract_url_without_regex($paragraph) {
    foreach (explode(" ", $paragraph) as $word) {
        if (strpos($word, "http") !== false && strpos($word, ".") !== false) {
            return $word;
        }
    }
    echo "URL blank. Not Found\n";
    return null;
}

$email = extract_email_address_without_regex($paragraph);
$phone_number = extract_phone_number_without_regex($paragraph);
$url = extract_url_without_regex($paragraph);

echo "\nEmail: $email\n";
echo "Phone number: $phone_number\n";
echo "URL: $url\n";

?>
