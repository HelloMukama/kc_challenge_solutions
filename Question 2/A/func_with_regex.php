<?php
// CHALLENGE QUESTION 2,A
// Author, NTARYEBWAMUKAMA JEDIDIAH

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

// Returning Email with Regex
function extract_email_address_with_regex($paragraph) {
    $regex_pattern = '/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/';
    // Use preg_match to find the first occurrence of the regex_pattern in the paragraph
    preg_match($regex_pattern, $paragraph, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}

// Returning Phone Number with Regex
function extract_phone_number_with_regex($paragraph) {
    $regex_pattern = '/\b\d{12}\b/';
    preg_match($regex_pattern, $paragraph, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}

// Returning Url with Regex
function extract_url_with_regex($paragraph) {
    $regex_pattern = '/\b(?:https?|ftp):\/\/\S+\b/i';
    preg_match($regex_pattern, $paragraph, $matches);
    return isset($matches[0]) ? $matches[0] : null;
}

$email_address = extract_email_address_with_regex($paragraph);
$phone_number = extract_phone_number_with_regex($paragraph);
$url = extract_url_with_regex($paragraph);

echo "Email Address: $email_address\n";
echo "Phone Number: $phone_number\n";
echo "URL: $url\n";

?>
