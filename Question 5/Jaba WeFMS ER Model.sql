-- CHALLENGE QUESTION 5,
-- Author, NTARYEBWAMUKAMA JEDIDIAH

-- "Database Design for Jaba's WeFMS including an Entity-Relationship model (ER model):"

-- ER Model:

-- Entities:
1. Category:
   -- Attributes:
      * id (Primary Key)
      * category_name

2. Organization:
   -- Attributes:
      * id (Primary Key)
      * organization_name
      * category_id (Foreign Key referencing Category)

3. Project:
   -- Attributes:
      * id (Primary Key)
      * project_name
      * organization_id (Foreign Key referencing Organization)

4. Form:
   -- Attributes:
      * id (Primary Key)
      * form_name
      * organization_id (Foreign Key referencing Organization)

5. Field:
   -- Attributes:
      * id (Primary Key)
      * field_name
      * data_type
      * form_id (Foreign Key referencing Form)

6. StaffMember:
   -- Attributes:
      * id (Primary Key)
      * first_name
      * last_name

7. FormEntry:
   -- Attributes:
      * id (Primary Key)
      * entry_date
      * form_data
      * form_id (Foreign Key referencing Form)
      * staff_member_id (Foreign Key referencing StaffMember)

8. Participant:
   -- Attributes:
      * id (Primary Key)
      * form_id (Foreign Key referencing Form)
      * participant_type
      * location

-- Relationships:

1. Category - Organization (One-to-Many):
   -- A category can have many organizations, but each organization belongs to only one category.
   -- Foreign Key: category_id in Organization

2. Organization - Project (One-to-Many):
   -- An organization can have many projects, but each project belongs to only one organization.
   -- Foreign Key: organization_id in Project

3. Organization - Form (One-to-Many):
   -- An organization can have many forms, but a form belongs to only one organization.
   -- Foreign Key: organization_id in Form

4. Form - Field (One-to-Many):
   -- A form can have many fields, but a field belongs to only one form.
   -- Foreign Key: form_id in Field

5. Staff Member - Form (One-to-Many):
   -- A staff member can have many forms, but each form is associated with one staff member.
   -- Foreign Key: staff_member_id in Form


6. Staff Member - Form Entry (One-to-Many):
   -- A staff member can have many form entries, but each form entry is associated with one staff member.
   -- Foreign Key: staff_member_id in Form Entry

7. Form - Form Entry (One-to-Many):
   -- A form can have multiple entries, but each entry is associated with only one form.
   -- Foreign Key: form_id in Form Entry

8. Participant - Form (One-to-Many):
   -- A participant can be associated with multiple forms. Each form is linked to one participant.
   -- Foreign Key: form_id in Participant


-- JABA WeFMS ERD:

```
                              +----------------------+
                              |    Category          |
                              +----------------------+   1 
                              | id (PK)              |<---|
                              | category_name        |    |
                              +----------------------+    |
                                                          |
                              +----------------------+    |
                              |  Organization        |    |
                              +----------------------+    |
                        |--1->| id (PK)              |<------1---|
                        |     | organization_name    |    |      |
                        |     | category_id (FK)     |--*-|      |
                        |     +----------------------+           |
                        |                                        |
                        |     +----------------------+           |
                        |     |     Project          |           |
                        |     +----------------------+           |
                        |     | id (PK)              |           |
                        |     | project_name         |           |
                        ---*--| organization_id (FK) |           |
                              +----------------------+           |
                                                                 |
                              +----------------------+           |
                              |      Form            |           |
                              +----------------------+           |
                  |-1---|---> | id (PK)              |<----*---------------|
                  |     |     | form_name            |           |         |
                  |     |1    | organization_id (FK) |-----*-----|         |   
                  |     |     | staff_member_id (FK) |---*---|             |
                  |     |     +----------------------+       |             |
                  |     |                                    |             |   
                  |     |     +----------------------+       |             |
                  |     |     |      Field           |       |             |
                  |     |     +----------------------+       |             |
                  |     |     | id (PK)              |       |             |
                  |     |     | field_name           |       |             |
                  |     |     | data_type            |       |             |
                  |     |---*-| form_id (FK)         |       |             |
                  |           +----------------------+       |             |
                  |                                          |             |
                  |           +----------------------+       |             |
                  |           |   StaffMember        |       |             |
                  |           +----------------------+       |             |
                  |    |--1-->| id (PK)              |<--1---|             |
                  |    |      | first_name           |                     |
                  |    |      | last_name            |                     |
                  |    |      +----------------------+                     |
                  |    |                                                   |
                  |    |      +----------------------+                     |
                  |    |      |    FormEntry         |                     |
                  |    |      +----------------------+                     |
                  |    |      | id (PK)              |                     |
                  |    |      | entry_date           |                     |
                  |    |      | form_data            |                     |
                  |    |---*--| staff_member_id (FK) |                     |
                  |--------*--| form_id (FK)         |                     |
                              +----------------------+                     |
                                                                           |
                              +----------------------+                     |
                              |   Participant        |                     |
                              +----------------------+                     |   
                              | id (PK)              |                     |  
                              | form_id (FK)         |--------1------------|  
                              | participant_type     |
                              | location             |
                              +----------------------+




Above is a sample ER Model Visual Representation i.e. ER Diagram in ASCII Characters.
Basically, an Organanisation (representing a NGO or private Company) has different individuals (Participants)
Different data can be colleceted by deferent StaffMembers with different forms.

Each StaffMember collects data of different datatypes using forms and the basic sample representation is shown above.
Ultimately, with an ER Model like this one, the StaffMembers at Jaba, ...
... thanks to the new Jaba WeFMS, will do away with the errors they originally faced in the data collection process.
This is because the Jaba WeFMS will follow this ER Model for its database schema design.




```
