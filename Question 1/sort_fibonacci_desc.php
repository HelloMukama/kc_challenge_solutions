<?php
// CHALLENGE QUESTION 1
// Author, NTARYEBWAMUKAMA JEDIDIAH

function my_fibonacci_sequence($n) {
    // initialize 0,1 array- fibonacci starting point
    $f = [0, 1];  
    
    for ($i = 2; $i < $n; $i++) {
        $f[$i] = $f[$i - 1] + $f[$i - 2];  // F(n)=F(n−1)+F(n−2)
    }
    return $f;
}

// n=13, Generate Fibonacci sequence up to the 13th term
$sample_fibonacci_numbers = my_fibonacci_sequence(13);  // 144 is the 13th number

// filter out Fibonacci numbers less than 100 (from the numbers 0 1 1 2 3 5 8 13 21 34 55 89 144)
// the variable '$fibonacci_lth_ahundred' below is ideal since 100 isn't a Fibonacci Number
$fibonacci_lth_ahundred = array_filter($sample_fibonacci_numbers, function ($value) {
    return $value <= 100;
});

// sort 0 to 100 Fibonacci - DESC 
rsort($fibonacci_lth_ahundred);

// Slice Array from 6th index i.e 7th number.
$final_result = array_slice($fibonacci_lth_ahundred, 6);

// Print the result
echo "\nPrinting array of fibonacci numbers lt 100 in desc, starting from the seventh number:\n";
print_r($final_result);

?>
