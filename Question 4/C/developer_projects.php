<?php
require_once '../db_connection.php';

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(['error' => 'Unsupported method']);
    exit();
}

// Validate and sanitize input data
$developerId = filter_input(INPUT_POST, 'developer_id', FILTER_VALIDATE_INT);

// Check for empty or invalid fields
if ($developerId === false) {
    echo json_encode(['error' => 'Attention! One or more values are missing or invalid']);
    exit();
}

try {
    // Verify that the entered developer_id exists
    $stmtDeveloperCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE id = ? AND role = 'developer'");
    $stmtDeveloperCheck->execute([$developerId]);
    $developerCount = $stmtDeveloperCheck->fetchColumn();

    if ($developerCount === 0) {
        // If the specified developer id does not exist or is not an developer, raise an error
        echo json_encode(['error' => 'Invalid entry. The specified developer id is not a valid developer.']);
        exit();
    }

    // Retrieve the list of projects assigned to the developer
    $stmtProjects = $pdo->prepare("SELECT name FROM projects WHERE developer_id = ?");
    $stmtProjects->execute([$developerId]);
    $projects = $stmtProjects->fetchAll(PDO::FETCH_ASSOC);

    // Respond with the list of projects
    echo json_encode(['projects' => $projects]);
} catch (PDOException $e) {
    // Respond with a standardized error message
    echo json_encode(['error' => 'Error retrieving projects: ' . $e->getMessage()]);
    exit();
}

/*
Enter various values of developer_id below to test out


TERMINAL
curl -X POST -d "developer_id=3" http://localhost/kc_task_manager/C/developer_projects.php

API CLIENT ----- JUST enter the necessary values and therir corresponding keys
http://localhost/kc_task_manager/C/developer_projects.php

*/

?>
