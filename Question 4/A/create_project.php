<?php
require_once '../db_connection.php';

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Validate input data
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
    $developerId = filter_input(INPUT_POST, 'developer_id', FILTER_VALIDATE_INT);
    $projectManagerId = filter_input(INPUT_POST, 'project_manager_id', FILTER_VALIDATE_INT);

    // Perform additional validation as needed
    if (!$name || !$status || $developerId === false || $projectManagerId === false) {
        echo json_encode(['error' => 'Attention! One or more values are missing or invalid']);
        exit();
    }

    try {
        // Check for duplicate entry
        $stmtCheck = $pdo->prepare("SELECT COUNT(*) FROM projects WHERE name = ? AND developer_id = ? AND project_manager_id = ?");
        $stmtCheck->execute([$name, $developerId, $projectManagerId]);
        $count = $stmtCheck->fetchColumn();

        if ($count > 0) {
            // Same combination of name, status, developer_id, and project_manager_id in a new entry should raise an error
            echo json_encode(['error' => 'Duplicate entry. This project already exists.']);
            exit();
        }

        // Validate that the entered developer_id is a valid user with the role 'developer'
        $stmtDeveloperCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE id = ? AND role = 'developer'");
        $stmtDeveloperCheck->execute([$developerId]);
        $developerCount = $stmtDeveloperCheck->fetchColumn();

        if ($developerCount === 0) {
            // If the entered developer_id is not a valid user with the role 'developer', raise an error
            echo json_encode(['error' => 'Invalid developer_id. The specified user is not a developer.']);
            exit();
        }

        // Validate that the entered project_manager_id is a valid user with the role 'project_manager'
        $stmtManagerCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE id = ? AND role = 'project_manager'");
        $stmtManagerCheck->execute([$projectManagerId]);
        $managerCount = $stmtManagerCheck->fetchColumn();

        if ($managerCount === 0) {
            // If the entered project_manager_id is not a valid user with the role 'project_manager', raise an error
            echo json_encode(['error' => 'Invalid project_manager_id. The specified user is not a project manager.']);
            exit();
        }

        // Use prepared statements to prevent SQL injection
        $stmt = $pdo->prepare("INSERT INTO projects (name, status, developer_id, project_manager_id) VALUES (?, ?, ?, ?)");
        $stmt->execute([$name, $status, $developerId, $projectManagerId]);

        // Respond with success or any other appropriate response
        echo json_encode(['message' => 'Project created successfully']);
        exit();
    } catch (PDOException $e) {
        // Respond with an error message
        echo json_encode(['error' => 'Error creating project: ' . $e->getMessage()]);
        exit();
    }
}

// Respond with an error for unsupported HTTP methods
echo json_encode(['error' => 'Unsupported method']);

/*
Ceate 3 sample projects via:-
TERMINAL / CMD
curl -X POST -d "name=KanzuCode Project1" -d "status=in-progress" -d "developer_id=3" -d "project_manager_id=1" http://localhost/kc_task_manager/A/create_project.php
curl -X POST -d "name=KanzuCode Project2" -d "status=awaiting-start" -d "developer_id=4" -d "project_manager_id=2" http://localhost/kc_task_manager/A/create_project.php
curl -X POST -d "name=KanzuCode Project3" -d "status=in-progress" -d "developer_id=5" -d "project_manager_id=2" http://localhost/kc_task_manager/A/create_project.php
curl -X POST -d "name=KanzuCode Project4" -d "status=on-hold" -d "developer_id=3" -d "project_manager_id=2" http://localhost/kc_task_manager/A/create_project.php

OR via a post request in say postman and send a POST request
Simply go to the end point http://localhost/kc_task_manager/A/create_project.php and submit corresponding data in the fields

*/
?>
