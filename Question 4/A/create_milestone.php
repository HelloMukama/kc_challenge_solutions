<?php
require_once '../db_connection.php';

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(['error' => 'Unsupported method']);
    exit();
}

// Validate and sanitize input data
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$projectId = filter_input(INPUT_POST, 'project_id', FILTER_VALIDATE_INT);
$developerId = filter_input(INPUT_POST, 'developer_id', FILTER_VALIDATE_INT);

// Check for empty or invalid fields
if (empty($name) || $projectId === false || $developerId === false) {
    echo json_encode(['error' => 'Attention! One or more values are missing or invalid']);
    exit();
}

// Use FILTER_SANITIZE_STRING for notes since it's a TEXT field
$notes_ = filter_input(INPUT_POST, 'notes', FILTER_SANITIZE_STRING);

try {
    // Check for duplicate entry
    $stmtCheck = $pdo->prepare("SELECT COUNT(*) FROM milestones WHERE name = ? AND project_id = ? AND developer_id = ? AND notes = ?");
    $stmtCheck->execute([$name, $projectId, $developerId, $notes_]);
    $count = $stmtCheck->fetchColumn();

    if ($count > 0) {
        // Same combination of name, project_id, developer_id, and notes in a new entry should raise an error
        echo json_encode(['error' => 'Duplicate entry. This milestone already exists.']);
        exit();
    }

    // Validate that the entered developer_id is a valid user
    $stmtIdCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE id = ?");
    $stmtIdCheck->execute([$developerId]);
    $idCount = $stmtIdCheck->fetchColumn();

    // Validate that the entered developer_id has the role 'developer'
    $stmtRoleCheck = $pdo->prepare("SELECT role FROM users WHERE id = ? AND role = 'developer'");
    $stmtRoleCheck->execute([$developerId]);
    $userRole = $stmtRoleCheck->fetchColumn();

    if ($userRole !== 'developer' && $idCount === 0) {
        // If the user's role is not 'developer', raise an error
        echo json_encode(['error' => 'Invalid entry. The specified developer id does not exist.']);
        exit();
    }

    // Use prepared statements to prevent SQL injection
    $stmt = $pdo->prepare("INSERT INTO milestones (name, project_id, developer_id, notes) VALUES (?, ?, ?, ?)");
    $stmt->execute([$name, $projectId, $developerId, $notes_]);

    // Respond with success or any other appropriate response
    echo json_encode(['message' => 'Milestone created successfully']);
} catch (PDOException $e) {
    // Respond with an error message
    echo json_encode(['error' => 'Error creating milestone: ' . $e->getMessage()]);
}

// Respond with an error for unsupported HTTP methods
echo json_encode(['error' => 'Unsupported method']);


/*
TERMINAL ~ CMD
curl -X POST -d "name=Project Milestone1&project_id=2&developer_id=5&notes=Milestone notes v1" http://localhost/kc_task_manager/A/create_milestone.php
curl -X POST -d "name=Project Milestone2&project_id=1&developer_id=2&notes=Milestone notes v2" http://localhost/kc_task_manager/A/create_milestone.php
curl -X POST -d "name=Project Milestone3&project_id=2&developer_id=5&notes=Milestone notes v3" http://localhost/kc_task_manager/A/create_milestone.php
curl -X POST -d "name=Project Milestone4&project_id=3&developer_id=4&notes=Milestone notes v4" http://localhost/kc_task_manager/A/create_milestone.php
curl -X POST -d "name=Project Milestone5&project_id=3&developer_id=3&notes=Milestone notes v5" http://localhost/kc_task_manager/A/create_milestone.php
curl -X POST -d "name=Project Milestone6&project_id=2&developer_id=3&notes=Milestone notes v6" http://localhost/kc_task_manager/A/create_milestone.php

Ceate 3 sample projects via:-
TERMINAL / CMD

OR via a post request in say postman and send a POST request
Simply go to the end point http://localhost/kc_task_manager/A/create_milestone.php and submit corresponding data in the fields

*/

?>
