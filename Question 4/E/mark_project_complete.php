<?php
require_once '../db_connection.php';

// Get user roles from the database
$stmtUserRoles = $pdo->query("SELECT id, role FROM users");
$userRoles = $stmtUserRoles->fetchAll(PDO::FETCH_ASSOC);

// Initialize arrays for project manager and developer IDs
$projectManagerIds = [];
$developerIds = [];

// Iterate through user roles to populate arrays
foreach ($userRoles as $user) {
    if ($user['role'] === 'project_manager') {
        $projectManagerIds[] = $user['id'];
    } elseif ($user['role'] === 'developer') {
        $developerIds[] = $user['id'];
    }
}

// Simulated user ID for testing purposes
$simulatedAuthUserId = filter_input(INPUT_POST, 'simulatedAuthUser', FILTER_VALIDATE_INT);

// Check if the simulatedAuthUser value is a valid user ID
if (!in_array($simulatedAuthUserId, array_merge($projectManagerIds, $developerIds))) {
    echo json_encode(['error' => 'Invalid user ID']);
    exit();
}

// Validate and sanitize input data
$projectId = filter_input(INPUT_POST, 'project_id', FILTER_VALIDATE_INT);

// Simulated constant, set to 1 to allow developer access
$devLoggedIn = 1;

// Check for empty or invalid fields
if ($projectId === false && $devLoggedIn===1 ) {
    echo json_encode(['error' => 'Attention! Project ID is missing or invalid']);
    exit();
}

// Check if the simulatedAuthUser is a project manager
if (in_array($simulatedAuthUserId, $projectManagerIds)  && $devLoggedIn===1 ) {
    // Verify that the entered project_id exists
    $stmtProjectCheck = $pdo->prepare("SELECT COUNT(*) FROM projects WHERE id = ?");
    $stmtProjectCheck->execute([$projectId]);
    $projectCount = $stmtProjectCheck->fetchColumn();

    if ($projectCount === 0 && $devLoggedIn===1 ) {
        // If the specified project id does not exist, raise an error
        echo json_encode(['error' => 'Invalid entry. The specified project id does not exist.']);
        exit();
    }

    // Mark the project as completed
    $stmtMarkCompleted = $pdo->prepare("UPDATE projects SET status = 'completed' WHERE id = ?");
    $stmtMarkCompleted->execute([$projectId]);

    // Respond with success or any other appropriate response
    echo json_encode(['message' => 'Project marked as completed successfully']);
} elseif (in_array($simulatedAuthUserId, $developerIds) && $devLoggedIn===0 ) {
    // If the simulatedAuthUser is a developer, raise an error
    echo json_encode(['error' => 'Developers that are logged in can\'t change a project status to complete']);
    exit();
} else {
    // If the simulatedAuthUser is not found in projectManagerIds or developerIds, raise an error
    echo json_encode(['error' => "You're not logged in"]);
    exit();
}


/*

Any value supplied below to the 'simulatedAuthUser' is a simulation here simulatng an id of the logged in user.
To toggle login and logout, try changing  $devLoggedIn to 0 to simulate absence of a session.

TERMINAL / CMD
curl -X POST -d "simulatedAuthUser=3&project_id=3" http://localhost/kc_task_manager/E/mark_project_complete.php

API CLIENT ----- JUST enter the necessary values and therir corresponding keys
http://localhost/kc_task_manager/E/mark_project_complete.php

*/
