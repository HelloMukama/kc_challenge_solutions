<?php
session_start();

// Check if the user is already logged in
if (!isset($_SESSION['user'])) {
    // Redirect to the dashboard or another appropriate page
    header('Location: index.php');
    exit();
}

// Kanzu Code Task Manager -- API Endpoints
$apiEndpoints = [
    '/kc_task_manager/authenticate/apis/create_user.php' => 'POST - Create a new user',
    '/kc_task_manager/authenticate/apis/list_users.php' => 'GET - Create a new user',
    // A
    '/kc_task_manager/A/create_milestone.php' => 'POST - Create a new Milestone',
    '/kc_task_manager/A/create_project.php' => 'POST - Create a new project',
    // B
    '/kc_task_manager/B/update_project_status.php' => 'PUT - Update project status',
    // C
    '/kc_task_manager/C/developer_projects.php' => 'POST - Get / List projects that belong to a certain Developer',
    // D
    '/kc_task_manager/D/switch_developer.php' => 'PUT - Change / update developer on a project',
    // E
    '/kc_task_manager/E/mark_project_complete.php' => 'PUT - Change / update developer on a project',
];


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container mt-5">

        <div class="alert alert-success" role="alert">
            Welcome, <?= $_SESSION['user']['username']; ?>! (Role: <?= $_SESSION['user']['role']; ?>)
            <a href="logout.php" class="btn btn-danger float-right">Logout</a>
        </div>

        <h4>KANZU CODE TASK MANAGER- API Endpoints</h4><br>
        <ul class="list-group">
            <?php foreach ($apiEndpoints as $endpoint => $description): ?>
                <li class="list-group-item">
                    <strong><?= $endpoint; ?></strong> - <?= $description; ?>
                </li>
            <?php endforeach; ?>
        </ul>

    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
