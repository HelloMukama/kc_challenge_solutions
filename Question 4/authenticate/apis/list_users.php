<?php

require_once '../../db_connection.php';

// Check if the request is a GET request
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    try {
        // Use prepared statements to prevent SQL injection
        $stmt = $pdo->prepare("SELECT id, username, role FROM users");
        $stmt->execute();

        // Fetch all users as an associative array
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Return the list of users as JSON
        header('Content-Type: application/json');
        echo json_encode(['status' => 'success', 'users' => $users]);
        exit();
    } catch (PDOException $e) {
        // Return an error response
        header('Content-Type: application/json');
        echo json_encode(['status' => 'error', 'message' => 'Error retrieving users: ' . $e->getMessage()]);
        exit();
    }
} else {
    // Return an error response for non-GET requests
    header('Content-Type: application/json');
    echo json_encode(['status' => 'error', 'message' => 'Invalid request method']);
    exit();
}
 

/*
You can view the list of registered users with a GET request via the endpoints below

BROWSER / API CLIENT LIKE POSTMAN
http://localhost/kc_task_manager/authenticate/apis/list_users.php

TERMINAL ~ CMD
curl http://localhost/kc_task_manager/authenticate/apis/list_users.php

You should see 5 users from the database at the end of this. 2 managers. 3 developers. as per the resulting JSON Response
 */
?>
