<?php

require_once '../../db_connection.php';

// Check if the request is a POST request
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Extract the data from the POST request
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = $_POST['role'];

    try {
        // Check if the username already exists
        $stmtCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE username = ?");
        $stmtCheck->execute([$username]);
        $count = $stmtCheck->fetchColumn();

        if ($count > 0) {
            // Return an error response if the username already exists
            header('Content-Type: application/json');
            echo json_encode(['status' => 'error', 'message' => 'User with the specified username already exists']);
            exit();
        }

        // Use prepared statements to prevent SQL injection
        $stmt = $pdo->prepare("INSERT INTO users (username, password, role) VALUES (?, ?, ?)");
        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        $stmt->execute([$username, $hashedPassword, $role]);

        // Return a success response
        header('Content-Type: application/json');
        echo json_encode(['status' => 'success', 'message' => 'User created successfully']);
        exit();
    } catch (PDOException $e) {
        // Return an error response for other errors
        header('Content-Type: application/json');
        echo json_encode(['status' => 'error', 'message' => 'Error creating user: ' . $e->getMessage()]);
        exit();
    }
} else {
    // Return an error response for non-POST requests
    header('Content-Type: application/json');
    echo json_encode(['status' => 'error', 'message' => 'Invalid request method']);
    exit();
}



/*
In your terminal, navigate to this file and follow the instructions below.
make a direct API POST request to the create user endpoint below.

copy and paste in terminal / cmd.

curl -X POST -d "username=JohnDoe&password=1234&role=project_manager" http://localhost/kc_task_manager/authenticate/apis/create_user.php
curl -X POST -d "username=Mutebi08&password=1234&role=project_manager" http://localhost/kc_task_manager/authenticate/apis/create_user.php
curl -X POST -d "username=JedidiahN&password=1234&role=developer" http://localhost/kc_task_manager/authenticate/apis/create_user.php
curl -X POST -d "username=Catherine09&password=1234&role=developer" http://localhost/kc_task_manager/authenticate/apis/create_user.php
curl -X POST -d "username=JaneDoe&password=1234&role=developer" http://localhost/kc_task_manager/authenticate/apis/create_user.php

You should be with 5 users in the database at the end of this. 2 managers. 3 developers as per the JSON Response
 */
?>
