<?php
session_start();

require_once '../../db_connection.php';

function isUserLoggedIn()
{
    return isset($_SESSION['user']);
}

function authenticateUser($username, $password, $pdo)
{
    $query = $pdo->prepare("SELECT * FROM users WHERE username = :username LIMIT 1");
    $query->bindParam(':username', $username);
    $query->execute();

    $user = $query->fetch(PDO::FETCH_ASSOC);

    if ($user && password_verify($password, $user['password'])) {
        $_SESSION['user'] = ['username' => $username, 'role' => $user['role']];
        return true;
    }

    return false;
}

// Check if the user is already logged in
if (isUserLoggedIn()) {
    // Return a message indicating the user is already logged in
    header('Content-Type: application/json');
    echo json_encode(['status' => 'success', 'message' => 'User already logged in']);
    exit();
}

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Check if the user exists before attempting to authenticate
    $userExistsQuery = $pdo->prepare("SELECT COUNT(*) FROM users WHERE username = :username");
    $userExistsQuery->bindParam(':username', $username);
    $userExistsQuery->execute();
    $userExists = (int)$userExistsQuery->fetchColumn();

    if ($userExists) {
        if (authenticateUser($username, $password, $pdo)) {
            // Return a success response for a successful login
            header('Content-Type: application/json');
            echo json_encode(['status' => 'success', 'message' => 'Login successful']);
            exit();
        } else {
            // Return an error response for an unsuccessful login
            header('Content-Type: application/json');
            echo json_encode(['status' => 'error', 'message' => 'Invalid username or password']);
            exit();
        }
    } else {
        // Return an error response if the user does not exist
        header('Content-Type: application/json');
        echo json_encode(['status' => 'error', 'message' => 'User does not exist']);
        exit();
    }
}

// Return an error response for unsupported HTTP methods or missing parameters
header('Content-Type: application/json');
echo json_encode(['status' => 'error', 'message' => 'Invalid request']);
exit();





/*
PERFORM A LOGIN VIA AN API POST REQUEST WITH THE CURL BELOW VIA YOUR TERMINAL IN THIS DIRECTORY.

curl -X POST -d "username=admin" -d "password=admin" -d role="developer" http://localhost/kc_task_manager/authenticate/apis/login.php


curl -X POST -d "username=admin" -d "password=admin" http://localhost/kc_task_manager/authenticate/apis/login.php
 
curl -X POST -d "login=1&username=admin&password=admin" http://localhost/kc_task_manager/authenticate/apis/login.php



*/

?>
