<?php
// logout.php

session_start();

// Check if the user is logged in
if (isset($_SESSION['user'])) {
    // Unset all session variables
    session_unset();

    // Destroy the session
    session_destroy();

    // Return a success response for a successful logout
    header('Content-Type: application/json');
    echo json_encode(['status' => 'success', 'message' => 'Logout successful']);
    exit();
} else {
    // Return an error response if the user is not logged in
    header('Content-Type: application/json');
    echo json_encode(['status' => 'error', 'message' => 'User is not logged in']);
    exit();
}
