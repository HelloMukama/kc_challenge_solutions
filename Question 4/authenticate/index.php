<?php
require_once '../db_connection.php';

session_start();

function isUserLoggedIn()
{
    return isset($_SESSION['user']);
}

function authenticateUser($username, $password, $pdo)
{
    // Replace the following line with your actual database authentication logic
    // You might want to use prepared statements to prevent SQL injection

    $query = $pdo->prepare("SELECT * FROM users WHERE username = :username LIMIT 1");
    $query->bindParam(':username', $username);
    $query->execute();

    $user = $query->fetch(PDO::FETCH_ASSOC);

    if ($user && password_verify($password, $user['password'])) {
        $_SESSION['user'] = ['username' => $username, 'role' => $user['role']];
        return true;
    }

    return false;
}

// Check if the user is already logged in
if (isset($_SESSION['user'])) {
    // Redirect to the dashboard or another appropriate page
    header('Location: dashboard.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (authenticateUser($username, $password, $pdo)) {
        header('Location: dashboard.php');
        exit();
    } else {
        $loginError = 'Invalid username or password';
    }
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container mt-5">

        <div class="row">

            <div class="col-md-6">
                <h4>KANZU CODE TASK MANAGER- Web</h4><br>
                <h2>Login</h2>
                <?php if (isset($loginError)): ?>
                    <div class="alert alert-danger" role="alert"><?= $loginError; ?></div>
                <?php endif; ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary" name="login">Login</button>
                </form>
                <p class="mt-3">Don't have an account yet? <a href="register.php">Register</a></p>
            </div>

        </div>

    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
