<?php
session_start();

// Unset and destroy the user session
unset($_SESSION['user']);
session_destroy();

// Redirect to the login page
header('Location: index.php');
exit();
?>
