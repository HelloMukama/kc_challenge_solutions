<?php
require_once '../db_connection.php';

session_start();

// Check if the user is already logged in
if (isset($_SESSION['user'])) {
    header('Location: dashboard.php');
    exit();
}

// Handle registration form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['register'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $role = $_POST['role'];

    try {
        // Use prepared statements to prevent SQL injection
        $stmt = $pdo->prepare("INSERT INTO users (username, password, role) VALUES (?, ?, ?)");
        $hashed_password = password_hash($password, PASSWORD_BCRYPT);
        $stmt->execute([$username, $hashed_password, $role]);

        header('Location: index.php');
        exit();
    } catch (PDOException $e) {
        $registrationError = 'Error in registration: ' . $e->getMessage();
    }
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KC TASK MANAGER - Register</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

    <div class="container mt-5">

        <div class="row">

            <div class="col-md-6">
                <h4>KANZU CODE TASK MANAGER- Web</h4><br>

                <?php if (isset($registrationError)): ?>
                    <div class="alert alert-danger" role="alert"><?= $registrationError; ?></div>
                <?php endif; ?>
                
                <form action="" method="post">
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="role">Role:</label>
                        <select class="form-control" name="role" required>
                            <option value="developer">Developer</option>
                            <option value="manager">Project Manager</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success" name="register">Register</button>
                </form>
                <p class="mt-3">Already have an account? <a href="index.php">Login</a></p>
            </div>

        </div>

    </div>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
