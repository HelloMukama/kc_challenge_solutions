<?php
require_once '../db_connection.php';

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(['error' => 'Unsupported method']);
    exit();
}

// Validate and sanitize input data
$projectId = filter_input(INPUT_POST, 'project_id', FILTER_VALIDATE_INT);
$newDeveloperId = filter_input(INPUT_POST, 'developer_id', FILTER_VALIDATE_INT);

// Check for empty or invalid fields
if ($projectId === false || $newDeveloperId === false) {
    echo json_encode(['error' => 'Attention! One or more values are missing or invalid']);
    exit();
}

try {
    // Verify that the entered project_id exists
    $stmtProjectCheck = $pdo->prepare("SELECT COUNT(*) FROM projects WHERE id = ?");
    $stmtProjectCheck->execute([$projectId]);
    $projectCount = $stmtProjectCheck->fetchColumn();

    if ($projectCount === 0) {
        // If the specified project id does not exist, raise an error
        echo json_encode(['error' => 'Invalid entry. project_id error, check and try again.']);
        exit();
    }

    // Verify that the entered developer_id exists
    $stmtDeveloperCheck = $pdo->prepare("SELECT COUNT(*) FROM users WHERE id = ? AND role = 'developer'");
    $stmtDeveloperCheck->execute([$newDeveloperId]);
    $developerCount = $stmtDeveloperCheck->fetchColumn();

    if ($developerCount === 0) {
        // If the specified developer id does not exist or is not an developer, raise an error
        echo json_encode(['error' => 'Invalid entry. The specified developer id is not a valid developer.']);
        exit();
    }

    // Update the developer assigned to the project
    $stmtUpdateDeveloper = $pdo->prepare("UPDATE projects SET developer_id = ? WHERE id = ?");
    $stmtUpdateDeveloper->execute([$newDeveloperId, $projectId]);

    // Respond with success or any other appropriate response
    echo json_encode(['message' => 'Developer assigned to the project successfully']);
} catch (PDOException $e) {
    // Respond with a standardized error message
    echo json_encode(['error' => 'Error updating developer assignment: ' . $e->getMessage()]);
    exit();
}



/*
TERMINAL / CMD
curl -X POST -d "developer_id=3" http://localhost/kc_task_manager/D/switch_developer.php
curl -X POST -d "project_id=7" -d "developer_id=6" http://localhost/kc_task_manager/D/switch_developer.php

API CLIENT ----- JUST enter the necessary values and their corresponding keys
http://localhost/kc_task_manager/D/switch_developer.php

*/

?>
