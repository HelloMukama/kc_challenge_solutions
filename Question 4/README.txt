// CHALLENGE QUESTION 4 - readme file
// Author, NTARYEBWAMUKAMA JEDIDIAH

go to your xampp or lampp directory's htdocs, and create a sub-directory called kc_task_manager
With Apache and MySQL running,

- start by creating a user in the database.
- Whilst at the root dir, cd /A/authenticate/apis/  <-- Instructions are inside

NB: You can run the cURL commands via terminal from any directory of your choice on your computer. Just make sure the Apache and MySQL servers are running.


------- ALTERNATIVELY ---------------


If you're on an API client, navigate to /authenticate/apis/index.php to access the web.

or just go to "http://localhost/kc_task_manager/authenticate/index.php"

Login process there is straightforward.
All the endpoints are on the dashboard after you login.

Thank you.

