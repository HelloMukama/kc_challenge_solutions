<?php
require_once '../db_connection.php';

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    echo json_encode(['error' => 'Unsupported method']);
    exit();
}

// Validate and sanitize input data
$projectId = filter_input(INPUT_POST, 'project_id', FILTER_VALIDATE_INT);
$newStatus = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);

// Check for empty or invalid fields
if ($projectId === false || empty($newStatus)) {
    echo json_encode(['error' => 'Attention! One or more values are missing or invalid']);
    exit();
}

try {
    // Check if the user is logged in and has the role 'developer'
    if (!isset($_SESSION['user'])) {
        echo json_encode(['error' => 'Permission denied. User is not logged in.']);
        exit();
    }

    if ($_SESSION['user']['role'] !== 'developer') {
        echo json_encode(['error' => 'Permission denied. User does not have the role of developer.']);
        exit();
    }

    // Verify that the entered project_id exists
    $stmtProjectCheck = $pdo->prepare("SELECT COUNT(*) FROM projects WHERE id = ?");
    $stmtProjectCheck->execute([$projectId]);
    $projectCount = $stmtProjectCheck->fetchColumn();

    if ($projectCount === 0) {
        // If the specified project id does not exist, raise an error
        echo json_encode(['error' => 'Invalid entry. The specified project id does not exist.']);
        exit();
    }

    // Respond with success or any other appropriate response
    echo json_encode(['message' => 'Project status updated successfully']);
} catch (PDOException $e) {
    // Respond with an error message
    echo json_encode(['error' => 'Error updating project status: ' . $e->getMessage()]);
    exit();
}

// Respond with an error for unsupported HTTP methods
echo json_encode(['error' => 'Unsupported method']);




/*

curl -X POST -d "project_id=7" -d "developer_id=5" -d "status=on-hold" http://localhost/kc_task_manager/B/update_project_status.php

curl -X POST -d "project_id=11" -d "status=in-progress" -b "PHPSESSID=$(php -r 'echo session_id();')" http://localhost/kc_task_manager/B/update_project_status.php


OR via a post request in say postman and send a POST request
Simply go to the end point http://localhost/kc_task_manager/B/update_project_status.php and submit corresponding data in the fields

*/











?>
